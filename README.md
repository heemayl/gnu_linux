## Repository for all standalone GNU/Linux stuffs, mostly scripts.

- `wrapserv`: Script for running "service action" commands (e.g. `docker restart`) with
  	      all the init specific commands/arguments anstracted away. Works with systemd,
	      upstart, sysv inits. Example: `wrapserv docker restart`, `wrapserv nginx status`.
	      Check `wrapserv --help`.
	     
	      